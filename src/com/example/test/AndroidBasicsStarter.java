package com.example.test;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class AndroidBasicsStarter extends ListActivity {

	String tests[] = { "LifeCycleTest", "SingleTouchTest", "MultiTouchTest",
					   "KeyTest", "AccelerometerTest", "AssetsTest",
					   "ExternalStorageTest", "SoundPoolTest", "MediaPlayerTest",
					   "FullScreenTest", "RenderViewTest", "ShapeTest", "BitmapTest",
					   "FontTest", "SurfaceViewTest"};
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new ArrayAdapter<String>(this,
        			  android.R.layout.simple_list_item_1, tests));
    //    setContentView(R.layout.activity_android_basics_starter);
    }

    protected void OnListItemClick(ListView list, View view, int position, long id)
    {
    	super.onListItemClick(list, view, position, id);
    	String testName = tests[position];
    	
    	try
    	{
    		Class clazz = Class.forName("com.example.test." + testName);
    		Intent intent = new Intent(this, clazz);
    		startActivity(intent);
    	}
    	catch(ClassNotFoundException e)
    	{
    		e.printStackTrace();
    	}
    }
    
}